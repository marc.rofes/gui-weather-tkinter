""" This app inform in real time the weather """
""" The modules I need are: Tkinter(for create the gui) """
import tkinter as tk
from tkinter import ttk
""" Pil for display images """
from PIL import ImageTk, Image
""" Requests, json and urlib for use the API """
import requests
import json
from urllib.request import urlretrieve
from urllib.parse import quote
import os
""" BytesIO for transform the bytes that I get from the request of icon imatge into an usable imatge for Tkinter """
from io import BytesIO

def currentWeather(cityname):
    """ Allow know in real time the weather of a city """
    currentweather = tk.Tk()
    """ Title of windows """
    currentweather.title("El Temps en temps real")
    """ WORKING WITH API AND CONVERTING TO JSON """
    url_endpoint = 'http://api.openweathermap.org/data/2.5/weather?'
    mydict = {'q':cityname.lower(), 'appid':'b4ffcfd06a5a9f6f28675e79941c9eab', 'lang':'ca', 'units':'metric'}
    response = requests.get(url_endpoint, params=mydict)
    json = response.json()
    """ FORMATTING THE ANSWER """
    icon = json['weather'][0]['icon']
    description = json['weather'][0]['description']
    temperature = json['main']['temp']
    sensacio = json['main']['feels_like']
    pressure = json['main']['pressure']
    humidity = json['main']['humidity']

    """ SET THE BACKGROUND """
    """ Defining the size of window """
    canvas_current = tk.Canvas(currentweather, width=700, height=500)
    """ Set the background image """
    image=ImageTk.PhotoImage(Image.open("background.png"), master=currentweather)
    canvas_current.create_image(0,0, anchor=tk.NW, image=image)
    canvas_current.pack()

    """ SHOW THE INFORMATION IN THE WINDOW """
    icon_url = "https://openweathermap.org/img/wn/{}@2x.png".format(icon)
    response = requests.get(icon_url)
    img_data = response.content
    icon = ImageTk.PhotoImage(Image.open(BytesIO(img_data)), master=currentweather)
    panel = canvas_current.create_image(300, 100, image=icon, anchor=tk.NW)

    text_description= canvas_current.create_text(330,200,fill="white",font="Times 20 italic bold",
                        text="Descripció: {}".format(description))
    text_temperatura = canvas_current.create_text(350,230,fill="white",font="Times 20 italic bold",
                        text="Temperatura: {} °C".format(temperature))
    sensacio_termica = canvas_current.create_text(350,260,fill="white",font="Times 20 italic bold",
                        text="Sensació Termica: {} °C".format(sensacio))
    pressio = canvas_current.create_text(350,290,fill="white",font="Times 20 italic bold",
                        text="Pressió: {} hPa".format(pressure))
    humitat = canvas_current.create_text(350,320, fill="white",font="Times 20 italic bold",
                        text="Humitat: {}%".format(humidity))
    panel.pack()
    currentweather.mainloop()

def Now():
    """ Initialitze the app """
    now = tk.Tk()
    """ Defining the size of window """
    canvas_now = tk.Canvas(now, width=700, height=500)
    """ Set the background image """
    image=ImageTk.PhotoImage(Image.open("background.png"), master=now)
    canvas_now.create_image(0,0, anchor=tk.NW, image=image)
    """ Title of windows """
    now.title("El Temps Actual")
    """ Input data for use correctly the api"""
    welcome = tk.Label(now, text = "Benvinguda Ariadna", anchor="center")
    welcome.pack()
    cityname = ttk.Entry(now)
    cityname.insert(0, "Introduix la ciutat")
    cityname.place(x=300, y=200)
    button = ttk.Button(now, text="Veure el temps d'avui", command=lambda: currentWeather(cityname.get()))
    button.place(x=300, y=220)
    canvas_now.pack()
    now.mainloop()

""" Create the variable that more later is being replaced """
cityname = ''
""" Initialitze the app """
home = tk.Tk()
""" Defining the size of window """
canvas= tk.Canvas(home, width=700, height=500)
""" Set the background image """
image=ImageTk.PhotoImage(Image.open("background.png"))
canvas.create_image(0,0, anchor=tk.NW, image=image)
""" Title of windows """
home.title("El Temps")

welcome = tk.Label(home, text = "Benvingut Ariadna", anchor="center")
welcome.pack()
""" Command for matching this window with the function Now """
humitat = canvas.create_text(350,150, fill="white",font="Times 20 italic bold",
                        text="Benvingut a la aplicació del temps en temps real")
button_now = ttk.Button(home, text="Veure el temps d'avui", command=Now)
button_now.place(x=300, y=250)
canvas.pack()
home.mainloop()