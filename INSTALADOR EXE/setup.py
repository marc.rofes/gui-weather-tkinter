import cx_Freeze
import sys
from PIL import ImageTk, Image
import requests
import json
from urllib.request import urlretrieve
from urllib.parse import quote
import os
from io import BytesIO

base = None

if sys.platform == 'win32':
    base = "Win32GUI"

executables = [cx_Freeze.Executable("Temps.py", base=base, icon="icon.ico")]

cx_Freeze.setup(
    name = "El Temps",
    options = {"build_exe": {"packages":["tkinter","PIL", "requests", "json", "urllib3", "io"], "include_files":["background.png", "icon.ico"]}},
    version = "0.01",
    description = "Una aplicació per sbaer el temps en temps real.",
    executables = executables
    )